<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col">
			<h1>Oops, the page you visited doesn't exist!</h1>
			<h3>Didn't find what you were looking for?</h3>
			<p>You may  have visited a page that does not exist, but while you are here, check out some other pages on our site.</p>
		</div>
	</div>
</div>
	
<?php get_footer(); ?>
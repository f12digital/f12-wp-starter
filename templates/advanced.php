<?php 
/* Template Name: Advanced Page */ 
get_header();
?>


<?php

// check if the flexible content field has rows of data
if( have_rows('modules') ):

     // loop through the rows of data
    while ( have_rows('modules') ) : the_row();

        if( get_row_layout() == 'content' ):

        	echo get_template_part('template-parts/content');
 			
        elseif( get_row_layout() == 'some-other-module' ): 


        endif;

    endwhile;

else :

    // no layouts found

endif;

?>


<?php get_footer(); ?>
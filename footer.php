<footer class="global-footer">

</footer>

<?php if( is_user_logged_in() && current_user_can( 'edit_posts' ) ) { ?>
<nav class="logged-in-nav">
	<ul>
		<li>Logged In:</li>
		<li><a href="<?php echo admin_url(); ?>">Admin</a></li>
		<?php edit_post_link( __( 'Edit Post' ), '<li>', '</li>' );?>
	</ul>
</nav>
<?php }; ?>

<?php wp_footer(); ?>
<?php echo get_field('end_scripts', 'options'); ?>
</body>
</html>
This theme uses the following tools:
Gulp
Sass
Bootstrap 4
Font Awesome 4


Required Plugins:
ACF Pro



Assets Files Structure:
- css
-- compiled css file

- fonts
-- Font Awesome font files

- scss
-- global
--- all .scss files for global styles
-- modules
--- all .scss files for reusable acf modules and template parts (sliders, forms, etc)
-- templates
--- all .scss files for page templates (front-page, 404, etc)
-- site-settings.scss (global variables)
-- style.scss (imports all scss files)

- vendor
-- bootstrap
-- font-awesome



Getting started with Gulp:
install NPM if you haven't already
run command: 
npm install gulp gulp-sass gulp-useref gulp-uglify gulp-jsbeautify gulp-if gulp-cache run-sequence gulp-clean-css gulp-cssbeautify pump
<?php $content = get_sub_field('content'); ?>
<div class="container module-content">
	<div class="row">
	    <div class="col">
	      <?php echo $content; ?>
	    </div>
	</div>
</div>
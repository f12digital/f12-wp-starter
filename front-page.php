<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col">
			<h1>Front Page</h1>
			<p>Pancetta corned beef bacon, brisket jerky spare ribs salami beef leberkas turducken filet mignon sirloin short loin prosciutto pig. Flank landjaeger ham hock pork loin brisket, meatball beef ribs sirloin. Bresaola meatball biltong leberkas beef ribs pork loin ball tip meatloaf bacon drumstick beef turkey. Shankle bacon andouille, ground round swine meatloaf pork kielbasa ham hock cupim pig rump filet mignon t-bone pork chop. Salami rump alcatra pork loin spare ribs drumstick.</p>
			<h2><i class="fa fa-angle-down"></i></h2>
		</div>
	</div>
</div>
<?php get_footer(); ?>